
package Modelo;

public class Cotizacion {
    private String numDeCotizacion;
    private String descripcion;
    private float precio;
    private float porcentajeDePago;
    private int plazo;
    
    public Cotizacion(){
        
    }
    
    Cotizacion(String numDeCotizacion,String descripcion,float precio,float porcentajeDePago,int plazo){
        this.numDeCotizacion = numDeCotizacion;
        this.descripcion = descripcion;
        this.precio = precio;
        this.porcentajeDePago = porcentajeDePago;
        this.plazo = plazo;
    }

    Cotizacion(Cotizacion C){
        this.numDeCotizacion = C.numDeCotizacion;
        this.descripcion = C.descripcion;
        this.precio = C.precio;
        this.porcentajeDePago = C.porcentajeDePago;
        this.plazo = C.plazo;
    }

    public String getNumDeCotizacion() {
        return numDeCotizacion;
    }

    public void setNumDeCotizacion(String numDeCotizacion) {
        this.numDeCotizacion = numDeCotizacion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public float getPorcentajeDePago() {
        return porcentajeDePago;
    }

    public void setPorcentajeDePago(float porcentajeDePago) {
        this.porcentajeDePago = porcentajeDePago;
    }

    public int getPlazo() {
        return plazo;
    }

    public void setPlazo(int plazo) {
        this.plazo = plazo;
    }
    
    public float cacularPagoIncial(){
        return this.precio*(this.porcentajeDePago/100);
    }
    
    public float cacularPagoTotal(){
        return this.precio-cacularPagoIncial();
    }
    
    public float cacularPagoMensual(){
        return cacularPagoTotal()/getPlazo();
    }

}
